var Class = require('./class.js');
var Logger = require('./logger.js');
var Level = require('./level.js');

/// This cache uses simple LRU, meaning inserts are O(n)
/// MAX_ITEM_COUNT is the size of the cache.
/// MAX_AGE_IN_SEC is the maximum age of an item that the cache will return, in seconds.
var CacheManager = Class.extend({
	init: function() 
	{
		this._cache = {};
		this._itemCount = 0;
	},

	lookup: function(key) 
	{
		var entry = this._cache[key];
		if (!entry)
		{
			return null;
		}
		
		if (this._currentTime - entry.lastAccess < this.MAX_AGE_IN_SEC * 1000)
		{
			this._itemCount--;
			delete this._cache[key];
			return null;
		}
		
		entry.lastAccess = this._currentTime();
		return entry.value;
	},

	store: function(key, value) 
	{
		if (!this._cache.hasOwnProperty(key))
		{
			this._itemCount++;
			this._evictItemIfNeeded();
		}
		this._cache[key] = {lastAccess: this._currentTime(), value: value};

	},
	
	// PRIVATE

	_evictItemIfNeeded: function()
	{
		if (this._itemCount > this.MAX_ITEM_COUNT)
		{
			var lruKey = null; 
			var lruTime = Infinity;
			var tmpTime;

			for (var key in this._cache)
			{
				if ((tmpTime = this._cache[key].lastAccess) < lruTime)
				{
					lruKey = key;
					lruTime = tmpTime;
				}
			}
			delete this._cache[lruKey];
			
			this._itemCount--;
		}
	},

	_currentTime: function() 
	{
		return new Date().getTime();
	},

	_cache: null,
	_itemCount: null,
	MAX_ITEM_COUNT: 20,
	MAX_AGE_IN_SEC: 3600


});


module.exports = CacheManager;

