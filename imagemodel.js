var Class = require('./class.js');

var ImageModel = Class.extend({

	init: function(data)
	{
		this._width = data.w;
		this._height = data.h;
		this._mode = data.m ? data.m : "normal";
		this._url = data.u;
	},

	isValid: function()
	{
		return this._width != null && this._height != null && this._mode != null && this._url != null;
	},
	
	/// Returns a trace of the settings for the image to make it cacheable
	toString: function()
	{
		return this._width + '&' + this._height + '&' + this._mode + '&' + this._url;
	},

	// Accessors	
	width: function()
	{
		return this._width;
	},

	height: function()
	{
		return this._height;
	},
	
	mode: function()
	{
		return this._mode;
	},

	url: function()
	{
		return this._url;
	},

	// PRIVATE
	
	_width: null,
	_height: null,
	_mode: null,
	_url : null


});

module.exports = ImageModel;
