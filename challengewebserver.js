var Class = require('./class.js');
var Logger = require('./logger.js');
var Level = require('./level.js');
var ImageModel = require('./imagemodel.js');
var ImageMagick = require('./imagemagick.js');
var CacheManager = require('./cachemanager.js');
var http = require('http');
var url = require('url');


var ChallengeWebServer = Class.extend({
	/// Sets up the server.
	/// @param port The port to run on
	/// @param host The host
	init: function(port, host)
	{
		Logger.log(Level.DEBUG, "ChallengeWebServer#init", port, host);
		this._cache = new CacheManager();
		this._port = port;
		this._host = host;
	},

	/// Runs the server
	run: function()
	{
		Logger.log(Level.DEBUG, "ChallengeWebServer#run")
		var self = this;
		http.createServer(function(request, response){self.handleRequest(request, response);}).listen(this._port, this._host);
	},
	
	/// Callback to handle a request. Requires a specific query string format:
	/// http://localhost:3000/?w=800&h=600&u=http://upload.wikimedia.org/wikipedia/commons/0/0c/GoldenGateBridge-001.jpg
	/// Where w is width, h is height and u is the url for the image
	/// @param request
	/// @param response
	handleRequest: function(request, response)
	{
		Logger.log(Level.DEBUG, "ChallengeWebServer#handleRequest", request.url);
		var imageModel = new ImageModel(url.parse(request.url, true).query);
		if (!imageModel.isValid())
		{
			this.outputError(response, "404: Invalid query string.");
			return;
		}
		
		var cachedData = this._cache.lookup(imageModel.toString());
		var cachedMimeType = this._cache.lookup(imageModel.toString() + "-mime");
		if (cachedData && cachedMimeType)
		{
			Logger.log(Level.DEBUG, "ChallengeWebServer#handleRequest", "Cache hit (resized)");
			response.writeHead(200, {'Content-Type': cachedMimeType});
			response.end(cachedData, 'binary');
			return;
		}

		var self = this;
		var cachedUnresizedImage = this._cache.lookup(imageModel.url());
		var cachedUnresizedMimeType = this._cache.lookup(imageModel.url() + "-mime");
		if (cachedUnresizedImage && cachedUnresizedMimeType)
		{
			Logger.log(Level.DEBUG, "ChallengeWebServer#handleRequest", "Cache hit (unresized)");
			this.resizeImage(imageModel, cachedUnresizedMimeType, cachedUnresizedImage, response);
		}
		else
		{
			Logger.log(Level.DEBUG, "ChallengeWebServer#handleRequest", "Cache miss");
			http.get(imageModel.url(), function(imageResult) { 
					var arrayBuffers = [];
					// Grab the results and put them into a buffer.
					imageResult.on('data', function (chunk) {
						arrayBuffers.push(chunk);
					});
					imageResult.on('end', function() {
						var buffer = Buffer.concat(arrayBuffers);
						var mimeType = imageResult.headers['content-type'];
						self._cache.store(imageModel.url(), buffer);
						self._cache.store(imageModel.url() + "-mime", mimeType);
						self.resizeImage(imageModel, mimeType, buffer, response); 
					});
				}).
				on('error', function(e) {
					Logger.log(Level.WARNING, "ChallengeWebServer#handleRequest", "Fetch failed", e.message, imageModel.url());
					self.outputError(response, "404: Fetch failed - " + e.message);
				});
		}
	},

	/// Resizes a returned image and outputs it
	resizeImage: function(imageModel, mimeType, imageData, response)
	{
		Logger.log(Level.DEBUG, "ChallengeWebServer#resizeImage", imageModel, mimeType);
		var self = this;

		var extension;
		switch (mimeType)
		{
			default:
				Logger.log(Level.WARNING, "ChallengeWebServer#resizeImage", "Invalid mime type", mimeType);
				this.outputError(response, "404: Invalid mime type: " + mimeType);
				return;		
			
			case 'image/jpeg':
				extension = 'jpg';
				break;
		
			case 'image/gif':
				extension = 'gif';
				break;

			case 'image/png':
				extension = 'png';
				break;
		}
		response.writeHead(200, {'Content-Type': mimeType});

		// Once the full data is there, call ImageMagick#resize on it.
		var opts = {
			srcData: imageData,
			format: extension
		};

		if (imageModel.mode() == 'stretch')
		{
			Logger.log(Level.DEBUG, "ChallengeWebServer#resizeImage", "Stretch mode");
			opts.width = imageModel.width();
			opts.height = imageModel.height() + '!';	// The bang makes it so it can warp the image.
			self.imageMagickResizeAndCrop(imageModel, opts, response, 0, 0);
		}
		else
		{
			Logger.log(Level.DEBUG, "ChallengeWebServer#resizeImage", "Crop mode");
			ImageMagick.identify({data: imageData}, function(err, result) { 
				var cropWidth = 0;
				var cropHeight = 0;
				if (imageModel.width() / result.width > imageModel.height() / result.height)
				{
					Logger.log(Level.DEBUG, "ChallengeWebServer#resizeImage", "Wider than tall");
					opts.width = imageModel.width();
					cropHeight = imageModel.height();
				}
				else if (imageModel.height() / result.height > imageModel.width() / result.width)
				{
					Logger.log(Level.DEBUG, "ChallengeWebServer#resizeImage", "Taller than wide");
					cropWidth = imageModel.width();
					opts.height = imageModel.height();
				}
				else
				{
					opts.width = imageModel.width();
					opts.height = imageModel.height();
				}
				self.imageMagickResizeAndCrop(imageModel, opts, response, cropWidth, cropHeight);
			});
		}
	},

	// Calls out to ImageMagick
	imageMagickResizeAndCrop: function(imageModel, opts, response, cropWidth, cropHeight)
	{
		var self = this;
		ImageMagick.resize(
			opts,
			function(err, stdout, stderr)
			{
				if (err) 
				{ 
					Logger.log(Level.WARNING, "ChallengeWebServer#resizeImage", "ImageMagick error", err); 
					return;
				}

				if (cropWidth > 0)
				{
					var proc = ImageMagick.convert(['-', '-crop', '' + cropWidth + 'x' + opts.height + '+0+0', '-'], 0, 
						function(err, stdout, stderr) 
						{
							self._cache.store(imageModel.toString(), stdout);
							response.write(stdout, 'binary');
							response.end();
						}
					);
					proc.stdin.setEncoding('binary');
					proc.stdin.write(stdout, 'binary');
					proc.stdin.end();
				}
				else if (cropHeight > 0)
				{
					var proc = ImageMagick.convert(['-', '-crop', '' + opts.width + 'x' + cropHeight + '+0+0', '-'], 0, 
						function(err, stdout, stderr) 
						{
							self._cache.store(imageModel.toString(), stdout);
							response.write(stdout, 'binary');
							response.end();
						}
					);
					proc.stdin.setEncoding('binary');
					proc.stdin.write(stdout, 'binary');
					proc.stdin.end();
				}
				else
				{
					Logger.log(Level.INFO, "ChallengeWebServer#resizeImage", "Resize complete");
					self._cache.store(imageModel.toString(), stdout);
					response.write(stdout, 'binary');
					response.end();
				}
			}
		);

	},

	outputError: function(response, text)
	{
		Logger.log(Level.INFO, "ChallengeWebServer#outputError", text);
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end(text);
	},

	// PRIVATE

	_port: null,
	_host: null,
	_cache: null

});


module.exports = ChallengeWebServer;


