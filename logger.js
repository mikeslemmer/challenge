var Level = require('./level.js');

/// Static class that maintains an internal log level, and allows logging parameterized
/// based on that level.
var Logger = {
	/// Note, this can take multiple arguments.
	log: function(level)
	{
		if (level >= Logger._level)
		{
			console.log(Level.toString(level), Array.prototype.slice.call(arguments, 1))
		}

	},
	setLevel: function(level)
	{
		Logger._level = level;
	},
	level: function() 
	{
		return Logger._level;
	},

	// PRIVATE
	
	_level: Level.DEBUG
};

module.exports = Logger;
