var Level =
{
	DEBUG: 		1,
	INFO: 		2,
	WARNING:	3,
	CRITICAL:	4,

	/// Outputs a string level.
	toString: function(level)
	{
		switch(level)
		{
			default:		return "Undefined";
			case Level.DEBUG:	return "DEBUG";
			case Level.INFO:	return "INFO";
			case Level.WARNING:	return "WARNING";
			case Level.CRITICAL:	return "CRITICAL";
		}
	}
}

module.exports = Level;
